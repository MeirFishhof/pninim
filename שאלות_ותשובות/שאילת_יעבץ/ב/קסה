% שאלה קסה

[^1]*אחד* תבע לאחד בב"ד. שנעשה לו ערב בשביל אחר שלוה ממנו. ועכב לפרוע. ונגשו המלוה לפרעו. ובקש מזה שהיו בידו ספרים שלו בפקדון למשכון. ופייסו שישתיק מעליו נגישת ב"ח הנ"ל. שדוחקו ומבזה אותו בדו"ד. והלך זה שבידו הספרים ובקש מב"ח הלז שיניח ללוה הלז. ולא ידחקנו על הפרעון. והוא ערב בעדו בסך כך. ושוב בא הלוה בדברי ריצויים אל הערב שיחזיר לו הספרים. ועשה כן. ועכשיו תובע הב"ח סך המעות שאמר לו זה להיות ערב בעדו. ופסק המורה שישבע הערב שלא נעשה ערב. ואם לא ישבע. מחויב לשלם הסך ההוא. *נ"ל* לא טב הורה. דהא אינו ערב דשעת מתן מעות. גם לא פטר את הלוה בשביל ערבותו של זה. רק פייסו שלא ינגוש אותו. ועכשיו שמשטה בו (גלי דעתיה. דלא כיון רק לעשות נחת רוח לאוהבו הלוה. שתסור יד המלוה מעליו ולדחותו לפי שעה. סבר לאשתמוטי. דילמא בתוך כך מתרמו ליה זוזי ופרעיה) עדיין הוא עומד בתביעתו. יש לו עליו הכפיה של ב"ד כבראשונה. לא הזיקו ולא הפסידו בכך מאומה. לא היו אלא פטומי מילי בעלמא שימתין לו עוד. *ואם* אמנם בהגה בש"ע (סקכ"ט סע"ג) כתב י"א שאם היו לערב מעות של לוה בידו. נשתעבד בכל אופן. [^2]*הא* בד"מ השיג על מהרי"ו בזה. וה"ט דכתבו בהגה שבשם י"א משום דלא ברירא ליה. וא"כ הרי יכול הערב לומר לו קים לי דלא כוותיה מספיקא מי מפקינן מניה. וש"ך מוקי לה דווקא בשהיו אותם המעות עצמם שהלוה לו ביד הערב. ולפי זה אין מקום לחייב את הערב. ומעמד שלשתן לא היה בכאן. *אלא* שיש לי עיון בדעת הד"מ וש"ך הנ"ל. מאי קסברי הא ודאי דינא דמהרי"ו מוכרח מעצמו. דבכל גוונא מחייב ערב הלז. מדרבי נתן. ומאחר שהודה הלה שיש בידו מעות משל לוה. ודאי מבעי ליה לשלומי לב"ח שלו. יהו המעות איך שיהו. 

[^1] ערב דלאחר ממ"ע אע"פ שהיו בידו ספרי הלוה למשכון חוב של עצמו. מצי למימר משטה אני בך


[^2] השגה על בד"מ וש"ך בדין ערב הפוטר את הלוה כשיש לו ממנו מעות בידו


[^3]*ברם* הכא בנ"ד שהיו הספרים ממושכנים לנפקד. וב"ח קונה משכון. א"כ לא חל שעבודו של חוב אחר עליהם. כל זמן שלא נפרע הלה תחלה. והוא לא סילק שעבוד עצמו מעליהם. רק הניח דעתו של ב"ח זה שלא ירבה בנגישה כל כך. בפטומי מילי אשבח דעתיה. מדי ממשא אית בהו. מילי דכדי נינהו. ומצוה קעביד כ"ש אי איכא נמי הכחשה. מ"מ פשוט דאין כאן מקום לשבועה. כיון דכי קמודה נמי מיפטר זה ברור בעיני. גם עיקול לא נעשה על הספרים על פי ב"ד. לחייבו מתורת תקנה. יעב"ץ.

[^3] אך בנ"ו פשיטא אין כאן שבועה. דלא כפסק מורה זקן
